﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BrickArkanoid : MonoBehaviour
{
    public int lives;
 
    protected virtual void Start()
    {
        Colorear();
        lives = 2;
    }
   
    protected void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float r,g,b;
        r = Random.Range(0f, 1f);
        g = Random.Range(0f, 1f);
        b = Random.Range(0f, 1f);
 
        sp.color = new Color(r,g,b,1f);
    }
 
    public void TouchBall(){
        lives--;
        if(lives<=0){
            Debug.LogError("Me destruyo");
            Destroy(transform.gameObject);
        }else{
            Colorear();
        }
    }
}